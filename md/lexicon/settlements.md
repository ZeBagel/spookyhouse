# Settlements


### Hamlet
- Population [< 500]

### Village
- Population [500 - 2,500]
- A small community that clusters around a single point of interest, such as a church, or a local market.

### Town 
- Population [2,500 - 80,000]
