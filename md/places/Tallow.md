# Tallow

## Statistics
- Type: [Hamlet](../lexicon/settlements.md#hamlet)
- Population: 80 ish

## Description
Situated half way between [Edenbow](../places/Edenbow.md) and [Redwall](../places/Redwall.md) in a small valley. It has a tiny 1-story inn surrounded by a dozen homes. Just outside the town is a butcher, tanner, leatherworker and candle maker.

The workshops are located on the other side of a hill to help block the stench, next to a stream (don't drink the water). Primary industry is purchasing the livestock from Edenbow, processing it, and shipping the raw materials to Readwall

